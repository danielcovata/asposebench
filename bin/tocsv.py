#!/usr/bin/python

# requires python 3

import sys
import re


def main(args):
    if len(args) < 2:
        print("Usage: python tocsv.py <inputfile>")
        exit()
    infile = args[1]

    with open(infile) as f:
        print("threads,real,user,sys")
        for line in f:
            m = re.match(r'(\w+)\s+(\d+)m(\d+\.\d+)s', line)
            if m:
                category = m.group(1)
                minutes = float(m.group(2))
                seconds = float(m.group(3))
                total = minutes * 60.0 + seconds
                if category == "real":
                    #have to manually edit the cores for gathered data..
                    print("1,{}".format(total), end=",")
                elif category == "user":
                    print(total, end=",")
                else:
                    print(total)


if __name__ == "__main__":
    main(sys.argv)
