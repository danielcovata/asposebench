#!/usr/bin/env bash

# example 
# $ bash make_test_files.sh ~/financial.xls

if [[ $# < 1 ]]; then 
  echo "Need a template file to generate test files from"
  exit 1
fi
infile="$1"
out_folder=${2:-$infile}_out

rm -rf "$out_folder"
mkdir -p "$out_folder"
cp "$infile" "$out_folder"
cd "$out_folder"
for f in *; do cp "$f" "a.$f"; done
for f in *; do cp "$f" "b.$f"; done
for f in *; do cp "$f" "c.$f"; done
for f in *; do cp "$f" "d.$f"; done
for f in *; do cp "$f" "e.$f"; done
for f in *; do cp "$f" "f.$f"; done
for f in *; do cp "$f" "g.$f"; done

