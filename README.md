# covconv

Converts documents to PDF.

## Usage

You'll need to build covconv first.  For now that 
means building through intellij.  Just open
the project in Intellij and do a make.

Then run ./bin/publish <version>, eg

$ ./bin/publish 1.0.0

Which will create ./dist/covconv-1.0.0

Then just run it and follow the usage instructions:

$ cd dist/covconv-1.0.0
$ ./covconv

## Other stuff

There's a script to duplicate a file n times to
create a test file suite, you can use that 
to run performance tests on different files.
Currently you'll need to use 'time' to get metrics
