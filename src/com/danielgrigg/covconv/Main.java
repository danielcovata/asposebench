package com.danielgrigg.covconv;

import com.aspose.cells.MemorySetting;
import com.aspose.cells.PdfSaveOptions;
import com.aspose.cells.PrintingPageType;
import com.aspose.cells.Workbook;
import com.aspose.words.Document;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class Main {

    public static void ConvertDocument(File path, String outFolder) {
        int i = path.getName().lastIndexOf('.');
        String ext;
        if (i <= 0)  {
            System.out.println("no extension, no convert");
            return;
        }
        ext = path.getName().substring(i+1).toLowerCase();

        try {
            if (ext.equals("xls") || ext.equals("xlsx") || ext.equals("csv")) {
                PdfSaveOptions saveOpts = new PdfSaveOptions();
                saveOpts.setPrintingPageType(PrintingPageType.IGNORE_BLANK);
                saveOpts.setOnePagePerSheet(true);
                com.aspose.cells.LoadOptions loadOpts = new com.aspose.cells.LoadOptions();
                loadOpts.setMemorySetting(MemorySetting.MEMORY_PREFERENCE);

                PdfSaveOptions pdfSaveOptions = new PdfSaveOptions();
                pdfSaveOptions.setOnePagePerSheet(true);
                String outPath = new File(outFolder, path.getName()).getAbsolutePath() + ".pdf";
                Workbook workbook = new Workbook(path.getAbsolutePath());
                workbook.save(outPath, pdfSaveOptions);

            } else if (ext.equals("ppt") || ext.equals("pptx") || ext.equals("pos") || ext.equals("potx") || ext.equals("ppsx")) {

                com.aspose.slides.PdfOptions pdfSaveOptions = new com.aspose.slides.PdfOptions();
                pdfSaveOptions.setEmbedFullFonts(false);
                pdfSaveOptions.setEmbedTrueTypeFontsForASCII(true);
                pdfSaveOptions.setTextCompression(com.aspose.slides.PdfTextCompression.None);
                pdfSaveOptions.setJpegQuality((byte) 100);
                pdfSaveOptions.setCompliance(com.aspose.slides.PdfCompliance.Pdf15);
                pdfSaveOptions.setSaveMetafilesAsPng(true);
                pdfSaveOptions.setSufficientResolution(128);
//                pdfSaveOptions.setWarningCallback(new MyIWarningCallback());
                com.aspose.slides.Presentation pres = new com.aspose.slides.Presentation(path.getAbsolutePath());
                String outPath = new File(outFolder, path.getName()).getAbsolutePath() + ".pdf";
                pres.save(outPath, com.aspose.slides.SaveFormat.Pdf, pdfSaveOptions);
            } else if (ext.equals("doc") || ext.equals("docx") || ext.equals("rtf")) {
                Document doc = new Document(path.getAbsolutePath());
                String outPath = new File(outFolder, path.getName()).getAbsolutePath() + ".pdf";

                com.aspose.words.PdfSaveOptions options = new com.aspose.words.PdfSaveOptions();
                doc.save(outPath, options);
            } else if (ext.equals("png") || ext.equals("jpg") || ext.equals("tiff")) {
                System.out.println("Not supported yet");
            }
        }
        catch (Exception e) {
            System.err.format("conversion error: %s%n", e);
        }
    }

    public static void main(String[] args) {
        if (args.length < 4) {
            System.out.println("Usage: covconv inFolder outFolder poolSize licence-file");
            System.exit(1);
        }
        String docFolder = args[0];
        String outFolder = args[1];
        int poolSize = Integer.parseInt(args[2]);
        String licenceFile = args[3];

        try {
            com.aspose.slides.License slidesLicense = new com.aspose.slides.License();
            com.aspose.cells.License cellsLicense = new com.aspose.cells.License();
            com.aspose.words.License wordsLicense = new com.aspose.words.License();
            try {
                cellsLicense.setLicense(licenceFile);
                slidesLicense.setLicense(licenceFile);
                wordsLicense.setLicense(licenceFile);
            } catch (Exception e) {
                System.out.println("Unable to load Aspose security license" + e);
                return;
            }

            List<File> docPaths = new ArrayList<File>();

            File[] files = new File(docFolder).listFiles();
            if (files == null) {
                System.out.println("no files found in " + docFolder);
                System.exit(2);
            }

            for (File file : files) {
                if (file.isFile()) {
                    docPaths.add(file);
                }
            }

            final ExecutorService executor = Executors.newFixedThreadPool(poolSize);

            for (File inPath : docPaths) {
                Runnable r = () -> ConvertDocument(inPath, outFolder);
                executor.execute(r);
            }
            executor.shutdown();

            try {
                executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            } catch (InterruptedException e) {
                System.err.format("IOException: %s%n", e);
            }

        } catch (Exception e) {
            System.err.format("IOException: %s%n", e);
        }
    }

/*    private static class MyIWarningCallback implements IWarningCallback {
        @Override
        public int warning(IWarningInfo iWarningInfo) {
            System.out.println("warning " + iWarningInfo.getDescription());
            return 0;
        }
    }
    */
}
